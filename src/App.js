import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import SignUp from './pages/SignUp';

function App() {
  return (
    <div className="App">
      <SignUp />
    </div>
  );
}

export default App;
