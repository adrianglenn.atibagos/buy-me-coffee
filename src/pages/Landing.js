import React from 'react'
import Header from '../components/Header'

export default function Landing() {
  return (
    <>
      <Header />
      <div className='container-fluid g-1'>
        <div className='row text-center mt-5 justify-content-center'>
          <p className='fs-1 fw-bolder'>A supporter is worth a <br />thousand followers.</p>
          <p className='fs-6'>Accept donations. Start a membership, Sell anything you like. <br />It's easier thank you think.</p>
          <form className='d-flex border rounded-pill w-auto mt-5'>
            <div className="row g-0 align-items-center">
              <div className="col-auto d-flex">
                <label for="inputPassword6" className="col-form-label fw-bold">buymeacoffee.com/</label>
                <input type="password" id="inputPassword6" className="form-control border-0 ms-0" placeholder='yourname' />
              </div>
              <div className='col-auto'>
                <button className='btn btn-warning rounded-pill my-1 fw-bold' type='button'>Start my page</button>
              </div>
            </div>
          </form>
          <p className='fs-6 mt-4'>It's free, and takes less than a minute.</p>
        </div>
        <div className='bg-warning-subtle mt-5 text-center py-5'>
          <p className='fs-6 fw-bold pt-2'>DONATIONS</p>
          <p className='fs-1 fw-bold'>Give your audience an <br />easy way to say thanks.</p>
          <p className='fs-6'>Buy me a Coffee makes supporting fun and easy. <br /> In just a couple of taps.</p>
        </div>
      </div>
    </>
  )
}
