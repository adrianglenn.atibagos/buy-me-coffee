import React from 'react'
import HeaderSignUp from '../components/HeaderSignUp'

export default function SignUp() {
    return (
        <>
            <HeaderSignUp />
            <div className='container'>
                <div className='row justify-content-center text-center mt-5'>
                    <form style={{ 'width': '400px' }}>
                        <p className='fw-bold fs-1'>Sign up</p>
                        <input type='text' className='border border-2 border-dark form-control mt-5' placeholder='Email' />
                        <input type='password' className='border border-2 border-dark form-control mt-3' placeholder='Password' />
                        <button className='btn btn-warning fw-bold w-100 rounded-pill mt-5'>Continue with email</button>
                        <div class="line-with-text mt-3">
                            <hr />
                            <span class="text-muted fw-bold">or signup with</span>
                            <hr />
                        </div>
                        <div className='mt-4'>
                            <button className='border border-2 border-dark rounded-pill fw-bold py-2 w-100 bg-light' type='button'>
                                <div className='row'>
                                    <div className='col-4 text-end'>
                                        <img width="30" height="30" src="https://img.icons8.com/fluency/48/google-logo.png" alt="google-logo" />
                                    </div>
                                    <div className='col-8 text-start'>Sign up with Google</div>
                                </div>
                            </button>
                            <button className='border border-2 border-dark rounded-pill fw-bold py-2 w-100 bg-light mt-3' type='button'>
                                <div className='row'>
                                    <div className='col-4 text-end'>
                                        <img width="30" height="30" src="https://img.icons8.com/fluency/30/facebook-new.png" alt="facebook-new" />
                                    </div>
                                    <div className='col-8 text-start'>Sign up with Facebook</div>
                                </div>
                            </button>
                            <button className='border border-2 border-dark rounded-pill fw-bold py-2 w-100 bg-light mt-3' type='button'>
                                <div className='row'>
                                    <div className='col-4 text-end'>
                                        <img width="30" height="30" src="https://img.icons8.com/ios-filled/30/mac-os.png" alt="mac-os" />
                                    </div>
                                    <div className='col-8 text-start'>Sign up with Apple</div>
                                </div>
                            </button>
                            <button className='border border-2 border-dark rounded-pill fw-bold py-2 w-100 bg-light mt-3' type='button'>
                                <div className='row'>
                                    <div className='col-4 text-end'>
                                        <img width="30" height="30" src="https://img.icons8.com/color/30/twitter--v1.png" alt="twitter--v1" />
                                    </div>
                                    <div className='col-8 text-start'>Sign up with Twitter</div>
                                </div>
                            </button>
                        </div>
                        <div className='mt-3'>
                            <p className='fs-7' style={{fontSize: '13px'}}>By signing up, you agree to the <b>Terms</b> and <b>Privacy Policy</b>. <br />
                                Creators or content that violate our terms will be unpublished.
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </>
    )
}
