import React from 'react'
import coffee from '../coffee.png'

export default function HeaderSignUp() {
    return (
        <>
            <nav className="navbar navbar-expand px-5">
                <div className="container-fluid py-2">
                    <a className="navbar-brand" href="#">
                        <img src={coffee} height='30' width='30'></img>
                    </a>

                    <div className="ms-auto" id="navbarNav">
                        <ul className="navbar-nav nav-pills fw-bold text-dark">
                            <li className='nav-item'>
                                <a className='nav-link'>Already have an account?</a>
                            </li>
                            <li className="nav-item rounded-pill bg-none border border-2 border-dark">
                                <a className="nav-link" href="#">Login</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    )
}
