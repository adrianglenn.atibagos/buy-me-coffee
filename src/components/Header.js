import React from 'react'
import coffee from '../coffee.png'

export default function Header() {
    return (
        <>
            <nav className="navbar navbar-expand">
                <div className="container rounded-pill shadow-lg py-2">
                    <a className="navbar-brand" href="#">
                        <img src={coffee} height='30' width='30'></img>
                    </a>
                    <a className="nav-link fw-bold" href="#">FAQ</a>


                    <div className="ms-auto" id="navbarNav">
                        <ul className="navbar-nav nav-pills fw-bold text-dark">
                            <li className="nav-item">
                                <a className="nav-link" href="#">Log in</a>
                            </li>
                            <li className="nav-item rounded-pill bg-warning">
                                <a className="nav-link" href="#">Sign up</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    )
}
